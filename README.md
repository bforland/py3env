## Purpose

The goal of this simple project is to create a default python3 working repository for easy reproducablity. This allows for the use of pipenv mutiple projects without the need for an envirment for each, particularly useful if you use very similar packages for each project. For this reason the ```.gitignore``` is a little different. It ignores everything except for what is required for the environment. Anythings that is to be pushed to the git must be added.

## Installation

This project requires pipenv but pipenv requires pyenv for the python version management. Installation methods for these system packages are demonstrated below. To install the environment:  

```bash 
$ git clone git@gitlab.com:bforland/py3env.git
$ pipenv install
```

You can also brouse through the ```requirements.txt``` and add or remove packages you know you don't need and simply install this way:

```bash
$ pipenv install -r requirements.txt
```

## Requirements

There are many packages that are installed since this is designed to be a global pipenv, to easily generate the ```requirements.txt``` run:  
```bash
pipenv lock --requirements > requirements.txt
```

## System installations

### MacOS

TBA

### Linus

TBA

#### Notes

There is an issue with readline for MacOS. readline 7 is required for the shell to work properly. If homebrew is forcing readline 8, the lines below fix that.

```bash
cd /usr/local/opt/readline/lib/
cd /usr/local/opt/readline/lib/
sudo ln -s libreadline.8.0.dylib libreadline.7.dylib
```
